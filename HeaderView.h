//
//  HeaderView.h
//  Bactroban
//
//  Created by CuongPQ on 10/23/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionDelegate <NSObject>

-(void) ActionDelegate:(NSString *)state;

@end

@interface HeaderView : UIView

@property (nonatomic,strong) UILabel *leftLabel;

@property (nonatomic, strong) UILabel *header;

@property (nonatomic, strong) UILabel *subHeader;

@property (nonatomic, strong) UILabel *subHeaderNote;

@property (nonatomic, strong) UILabel *tradeMark;

@property (nonatomic, strong) UIButton *readingBtn;

@property (nonatomic, strong) UIButton *piBtn;

@property (nonatomic, strong) UIButton *homeBtn;

@property (nonatomic, assign) CGFloat heightVal;

@property (nonatomic, weak) id<ActionDelegate> actionDo;

-(CGFloat)updateCurrentHeightLayout;

-(void)resetBtnSate;

@end
