//
//  BasePopupView.h
//  Bactroban
//
//  Created by CuongPQ on 10/29/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BasePopupAction <NSObject>

-(void) TouchOutSidePopUpView;

@end

@interface BasePopupView : UIView

@property (nonatomic, strong) UIView *touchUpOutsideView;

@property (nonatomic, strong) UILabel *contentNote;

@property (nonatomic, weak) id<BasePopupAction> popupDelegate;

@end
