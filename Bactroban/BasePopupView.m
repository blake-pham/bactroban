//
//  BasePopupView.m
//  Bactroban
//
//  Created by CuongPQ on 10/29/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "BasePopupView.h"
#import "ConstVal.h"

@interface BasePopupView ()

@property (nonatomic,strong) UIView *bgView;

@end
@implementation BasePopupView

@synthesize touchUpOutsideView = _touchUpOutsideView;
@synthesize contentNote = _contentNote;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
        _touchUpOutsideView = [[UIView alloc] initWithFrame:frame];
        _touchUpOutsideView.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.3f];
        [_touchUpOutsideView addGestureRecognizer:singleTap];
        [self addSubview:_touchUpOutsideView];
        
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
        [_bgView setClipsToBounds:YES];
        _bgView.layer.cornerRadius = PADDING;
        [self addSubview:_bgView];
        
        _contentNote = [[UILabel alloc] initWithFrame:CGRectMake(0.0f,0.0f, self.frame.size.width/3.0f *2.0f, self.frame.size.height)];
        [_contentNote setTextColor:[UIColor colorWithRed:88.0f/255.0f green:89.0f/255.0f blue:91.0f/255.0f alpha:1.0f]];
        _contentNote.numberOfLines = 10;
        _contentNote.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:_contentNote];
        
    }
    return self;
}


-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    
    if (self.popupDelegate != nil && [self.popupDelegate respondsToSelector:@selector(TouchOutSidePopUpView)])
    {
        [self.popupDelegate TouchOutSidePopUpView];
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    self.touchUpOutsideView.frame = self.frame;
    
    [self.contentNote sizeToFit];
    self.contentNote.frame = CGRectMake((self.frame.size.width - self.contentNote.frame.size.width )/ 2.0f - PADDING , (self.frame.size.height - self.contentNote.frame.size.height)/ 2.0f - PADDING, self.contentNote.frame.size.width + PADDING  , self.contentNote.frame.size.height + PADDING);
    self.bgView.frame = CGRectMake(self.contentNote.frame.origin.x - PADDING, self.contentNote.frame.origin.y - PADDING, self.contentNote.frame.size.width + PADDING *2, self.contentNote.frame.size.height + PADDING*2);
}

@end
