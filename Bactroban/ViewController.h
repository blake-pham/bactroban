//
//  ViewController.h
//  Bactroban
//
//  Created by CuongPQ on 10/23/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"
#import "FooterView.h"
#import "MainContentView.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) HeaderView *headerViewLayout;
@property (nonatomic, strong) FooterView *footerViewLayout;
@property (nonatomic, strong) UIView *contentView;



-(void)initHomePage;

-(void)changeContent:(NSString *)state;
@end

