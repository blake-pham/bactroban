//
//  MainContentHeaderView.h
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContentActionDelegate <NSObject>

-(void) ActionContent:(NSString *)actionName;
-(void) ActionNote:(NSString *)noteContent;
-(void) SlideAction:(NSInteger )index withTitle:(NSString *)title;

@end

@interface MainContentView : UIView

@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIWebView *contentView;
@property (nonatomic, strong) UIButton *preBtn;
@property (nonatomic, strong) UIButton *nextBtn;
@property (nonatomic, strong) UIButton *noteBtn;
@property (nonatomic, strong) NSString *noteContent;
@property (nonatomic, assign) BOOL isSliding;

@property (nonatomic, weak) id<ContentActionDelegate> contentDelegate;
@property (nonatomic, weak) id<ContentActionDelegate> noteDelegate;
@property (nonatomic, weak) id<ContentActionDelegate> slideDelegate;

-(id)initWithSliding:(BOOL)enalbeSlide;
-(void)updateContent:(NSString *)content;
-(void)enableSlide:(BOOL)isEnalbe;
@end
