//
//  ProductView.m
//  Bactroban
//
//  Created by CuongPQ on 10/25/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "ProductView.h"
#import "ConstVal.h"

@implementation ProductView
//@synthesize productImage = _productImage;

@synthesize chiDinhBtn = _chiDinhBtn;

@synthesize coCheBtn = _coCheBtn;

@synthesize phoKhangBtn = _phoKhangBtn;

@synthesize tinhNhayCamBtn = _tinhNhayCamBtn;

@synthesize hieuQuaSoEryBtn = _hieuQuaSoEryBtn;

@synthesize hieuQuaSoGenBtn = _hieuQuaSoGenBtn;

@synthesize tacDungPhuBtn = _tacDungPhuBtn;

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
       /*
        _productImage = [UIImageView new];
        _productImage.image = [UIImage imageNamed:@"portrai_batroban.png"];
        [_productImage sizeToFit];
        [self addSubview:_productImage];
        */
        //self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _chiDinhBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_chiDinhBtn addTarget:self
                   action:@selector(productAction:)
         forControlEvents:UIControlEventTouchUpInside];
        [_chiDinhBtn setTitle:@"Chỉ định" forState:UIControlStateNormal];
        [_chiDinhBtn sizeToFit];
        [_chiDinhBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_chiDinhBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _chiDinhBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _chiDinhBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _chiDinhBtn.titleLabel.numberOfLines = 2;
        [_chiDinhBtn setTag:1];
        [self addSubview:_chiDinhBtn];
        
        _coCheBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_coCheBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_coCheBtn setTitle:CO_CHE_TAC_DUNG forState:UIControlStateNormal];
        [_coCheBtn sizeToFit];
        [_coCheBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_coCheBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _coCheBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _coCheBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _coCheBtn.titleLabel.numberOfLines = 2;
        [_coCheBtn setTag:2];
        [self addSubview:_coCheBtn];
        
        _phoKhangBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_phoKhangBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_phoKhangBtn setTitle:PHO_KHANG_KHUAN forState:UIControlStateNormal];
        [_phoKhangBtn sizeToFit];
        [_phoKhangBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_phoKhangBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _phoKhangBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _phoKhangBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _phoKhangBtn.titleLabel.numberOfLines = 2;
        [_phoKhangBtn setTag:3];
        [self addSubview:_phoKhangBtn];
        
        _tinhNhayCamBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_tinhNhayCamBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_tinhNhayCamBtn setTitle:TINH_NHAY_CAM_VI_KHUAN forState:UIControlStateNormal];
        [_tinhNhayCamBtn sizeToFit];
        [_tinhNhayCamBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_tinhNhayCamBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _tinhNhayCamBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _tinhNhayCamBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _tinhNhayCamBtn.titleLabel.numberOfLines = 2;
        [_tinhNhayCamBtn setTag:4];
        [self addSubview:_tinhNhayCamBtn];
        
        _hieuQuaSoEryBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_hieuQuaSoEryBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_hieuQuaSoEryBtn setTitle:HIEU_QUA_SO_VOI_ERYTHROMYCIN forState:UIControlStateNormal];
        [_hieuQuaSoEryBtn sizeToFit];
        [_hieuQuaSoEryBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_hieuQuaSoEryBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _hieuQuaSoEryBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _hieuQuaSoEryBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _hieuQuaSoEryBtn.titleLabel.numberOfLines = 2;
        [_hieuQuaSoEryBtn setTag:5];
        [self addSubview:_hieuQuaSoEryBtn];
        
        _hieuQuaSoGenBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_hieuQuaSoGenBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_hieuQuaSoGenBtn setTitle:HIEU_QUA_SO_VOI_GENTAMICIN forState:UIControlStateNormal];
        [_hieuQuaSoGenBtn sizeToFit];
        [_hieuQuaSoGenBtn setTintColor:[UIColor redColor]];
        [_hieuQuaSoGenBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_hieuQuaSoGenBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _hieuQuaSoEryBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _hieuQuaSoGenBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _hieuQuaSoGenBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _hieuQuaSoGenBtn.titleLabel.numberOfLines = 2;
        [_hieuQuaSoGenBtn setTag:6];
        [self addSubview:_hieuQuaSoGenBtn];
        
        _tacDungPhuBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_tacDungPhuBtn addTarget:self
                        action:@selector(productAction:)
              forControlEvents:UIControlEventTouchUpInside];
        [_tacDungPhuBtn setTitle:TAC_DUNG_KHONG_MONG_MUON forState:UIControlStateNormal];
        [_tacDungPhuBtn sizeToFit];
        [_tacDungPhuBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_tacDungPhuBtn setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        _hieuQuaSoEryBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _tacDungPhuBtn.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f];
        _tacDungPhuBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _tacDungPhuBtn.titleLabel.numberOfLines = 2;
        [_tacDungPhuBtn setTag:7];
        [self addSubview:_tacDungPhuBtn];
        
    }
    return self;
}

- (void)productAction:(id)sender
{
    NSInteger index = ((UIButton*) sender).tag;
    
    
    if (self.productDelegate != nil && [self.productDelegate respondsToSelector:@selector(FeatureAction:)] == YES)
    {
        [self.productDelegate FeatureAction:index];
    }
}

-(void) layoutSubviews
{
    //CGFloat width = self.frame.size.width;
    //CGFloat height = self.frame.size.height;
    
    /*self.productImage.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.productImage.frame.size.width, self.productImage.frame.size.height);
    */
    
    self.chiDinhBtn.frame = CGRectMake( 0.0f,205.0f, self.chiDinhBtn.frame.size.width, self.chiDinhBtn.frame.size.height);
    self.coCheBtn.frame = CGRectMake( 50.0f,35.0f, self.coCheBtn.frame.size.width, self.coCheBtn.frame.size.height);
    self.phoKhangBtn.frame = CGRectMake( 170.0f,60.0f, self.phoKhangBtn.frame.size.width, self.phoKhangBtn.frame.size.height);
    
    self.tinhNhayCamBtn.frame = CGRectMake( 310.0f,80.0f, self.tinhNhayCamBtn.frame.size.width, self.tinhNhayCamBtn.frame.size.height);
    
    //[self.hieuQuaSoEryBtn sizeThatFits:CGSizeMake(self.hieuQuaSoEryBtn.frame.size.width / 2.0f, self.hieuQuaSoEryBtn.frame.size.height *2)];
    
    self.hieuQuaSoEryBtn.frame = CGRectMake( 445.0f,180.0f, self.hieuQuaSoEryBtn.frame.size.width*2/3.0f, self.hieuQuaSoEryBtn.frame.size.height * 2.0f);
    
    self.hieuQuaSoGenBtn.frame = CGRectMake( 420.0f,320.0f, self.hieuQuaSoGenBtn.frame.size.width, self.hieuQuaSoGenBtn.frame.size.height);
    
    self.tacDungPhuBtn.frame = CGRectMake( 290.0f,415.0f, self.tacDungPhuBtn.frame.size.width, self.tacDungPhuBtn.frame.size.height);
    
    //self.frame = self.productImage.frame;
}
@end
