//
//  ConstVal.h
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#ifndef Bactroban_ConstVal_h
#define Bactroban_ConstVal_h

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define HEADER_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 240.0f : 320.0f)

#define FOOTER_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 40.0f : 50.0f)

#define BUTTON_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 48.0f : 64.0f)
#define BUTTON_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 48.0f : 64.0f)

#define PADDING ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 5.0f : 10.0f)

#define IMAGE_VIEW_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 60.0f : 80.0f)

#define IMAGE_VIEW_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? 20.0f : 30.0f)

#define HOME @"Home"

#define PI @"Pi"

#define READING @"Reading"

#define CHI_DINH @"Chỉ định"

#define CO_CHE_TAC_DUNG @"Cơ chế tác dụng phụ"

#define PHO_KHANG_KHUAN @"Phổ kháng khuẩn"

#define TINH_NHAY_CAM_VI_KHUAN @"Tính nhạy cảm của vi khuẩn"

#define HIEU_QUA_SO_VOI_ERYTHROMYCIN @"Hiệu quả so với Erythromycin dạng uống"

#define HIEU_QUA_SO_VOI_GENTAMICIN @"Hiệu quả so với Gentamicin"

#define TAC_DUNG_KHONG_MONG_MUON @"Tác dụng không mong muốn"

#define THAN_TRONG @"Thận trọng"

#define THAI_KI_VA_CHO_CON_BU @"Thai kì và cho con bú"



#endif
