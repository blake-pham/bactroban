//
//  ProductView.h
//  Bactroban
//
//  Created by CuongPQ on 10/25/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductActionDelegate <NSObject>

-(void)FeatureAction:(NSInteger)index;

@end

@interface ProductView : UIView

//@property (nonatomic, strong) UIImageView *productImage;

@property (nonatomic, strong) UIButton *chiDinhBtn;

@property (nonatomic, strong) UIButton *coCheBtn;

@property (nonatomic, strong) UIButton *phoKhangBtn;

@property (nonatomic, strong) UIButton *tinhNhayCamBtn;

@property (nonatomic, strong) UIButton *hieuQuaSoEryBtn;

@property (nonatomic, strong) UIButton *hieuQuaSoGenBtn;

@property (nonatomic, strong) UIButton *tacDungPhuBtn;

@property (nonatomic, weak) id<ProductActionDelegate> productDelegate;

@end
