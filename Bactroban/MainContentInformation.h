//
//  MainContent.h
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PIActionDelegate <NSObject>

-(void) ActionPIContent:(NSString *)actionName;

@end

@interface MainContentInformation : UIView

@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIWebView *contentView;
@property (nonatomic, weak) id<PIActionDelegate> contentDelegate;

-(void)updateContent:(NSString *)content;
@end
