//
//  FooterView.m
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "FooterView.h"
#import "ConstVal.h"
#import "Utils.h"

@implementation FooterView

@synthesize leftTitle = _leftTitle;
@synthesize contentLabel = _contentLabel;
@synthesize rightLabel = _rightLabel;

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _leftTitle = [UILabel new];
        [_leftTitle setText:@"Code: VN/MUP/0005/14"];
        _leftTitle.textColor = [UIColor blackColor];
        [_leftTitle setFont:[UIFont systemFontOfSize:12.0f]];
        [self addSubview:_leftTitle];
        
        _contentLabel = [UILabel new];
        [_contentLabel setText:@""];
        [_contentLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f]];
        _contentLabel.textColor = [UIColor blackColor];
        [self addSubview:_contentLabel];
        
        _rightLabel = [UILabel new];
        [_rightLabel setText:@""];
        [_rightLabel setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:12.0f]];
        _rightLabel.textColor = [UIColor blackColor];
        [self addSubview:_rightLabel];
    }
    //self.backgroundColor = [UIColor blueColor];
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGSize screenSize = [Utils screenSize];
    
    [self.contentLabel sizeToFit];
    self.contentLabel.frame = CGRectMake((width - self.contentLabel.frame.size.width) /2.0f, (height - self.contentLabel.frame.size.height) /2.0f, self.contentLabel.frame.size.width, self.contentLabel.frame.size.height);
    
    
    [self.leftTitle sizeToFit];
    self.leftTitle.frame = CGRectMake(PADDING, (height - self.leftTitle.frame.size.height)/ 2.0f, self.contentLabel.frame.origin.x - PADDING*2, self.leftTitle.frame.size.height);
    
    [self.rightLabel sizeToFit];
    self.rightLabel.frame = CGRectMake(self.contentLabel.frame.origin.x + self.contentLabel.frame.size.width + PADDING, (height - self.rightLabel.frame.size.height)/2.0f, width - self.contentLabel.frame.origin.x - self.contentLabel.frame.size.width - PADDING, self.rightLabel.frame.size.height);
    
    self.frame = CGRectMake(0, screenSize.height - self.contentLabel.frame.size.height - PADDING*3, width, self.contentLabel.frame.size.height + PADDING*2);
}

-(void) updateContent:(NSString *)leftContent contentData:(NSString *)content right:(NSString *)rightContnet
{
    
}

@end
