//
//  Utils.h
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (UIImage *)imageWithColor:(UIColor *)color withFrame:(CGRect)frame;

+ (CGSize)screenSize;

@end
