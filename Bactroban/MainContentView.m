//
//  MainContentHeaderView.m
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "MainContentView.h"
#import "Utils.h"
#import "ConstVal.h"

@interface MainContentView()
@property (nonatomic, strong) UIView *bgColor;

@end

@implementation MainContentView

@synthesize leftImage = _leftImage;
@synthesize title = _title;
@synthesize closeBtn = _closeBtn;
@synthesize contentView = _contentView;
@synthesize preBtn = _preBtn;
@synthesize nextBtn = _nextBtn;
@synthesize noteBtn = _noteBtn;

-(id)initWithSliding:(BOOL)enalbeSlide
{
    _isSliding = enalbeSlide;
    return [self initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.0f)];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _bgColor = [[UIView alloc] initWithFrame:CGRectMake(PADDING*2, 0.0f, self.frame.size.width - PADDING*2, self.frame.size.height)];
        _bgColor.backgroundColor = [UIColor whiteColor];
        _bgColor.layer.cornerRadius = 30.0f;
        
        [self addSubview:_bgColor];
        
        _leftImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"content_sign.png"]];
        [self addSubview:_leftImage];
        
        _title =[UILabel new];
        [_title setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:25.0f]];
        [_title setTextColor:[UIColor redColor]];
        [self addSubview:_title];
        
        _closeBtn = [UIButton new];
        //width and height should be same value
        //_closeBtn.frame = CGRectMake(0, self.frame.origin.y, BUTTON_WIDTH, BUTTON_HEIGHT);
        //Clip/Clear the other pieces whichever outside the rounded corner
        _closeBtn.clipsToBounds = YES;
        [_closeBtn setImage:[UIImage imageNamed:@"closeBtn.png"] forState:UIControlStateNormal ];
        [_closeBtn setTag:1];
        [_closeBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        //half of the width
        _closeBtn.layer.cornerRadius = BUTTON_WIDTH/2.0f;
       // _closeBtn.layer.borderColor = (__bridge CGColorRef)([UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:161.0f/255.0f alpha:0.7f]);
        //_closeBtn.layer.borderWidth=2.0f;
        [self addSubview:_closeBtn];
        
        _contentView = [UIWebView new];
        _contentView.contentMode = UIViewContentModeScaleAspectFit;
        [_contentView setBackgroundColor:[UIColor clearColor]];
        _contentView.scrollView.scrollEnabled = YES;
        [self addSubview:_contentView];
        
        _preBtn = [UIButton new];
        [_preBtn setImage:[UIImage imageNamed:@"preBtn.png"] forState:UIControlStateNormal ];
        [_preBtn setImage:[UIImage imageNamed:@"preBtnSelected.png"] forState:UIControlStateHighlighted ];
        [_preBtn setImage:[UIImage imageNamed:@"preBtnSelected.png"] forState:UIControlStateSelected ];
        [_preBtn setTag:1];
        [_preBtn addTarget:self action:@selector(slideAction:) forControlEvents:UIControlEventTouchUpInside];
        _preBtn.titleLabel.text = @"previous";
        [_preBtn setHidden:YES];
        [self addSubview:_preBtn];
        
        _nextBtn = [UIButton new];
        [_nextBtn setImage:[UIImage imageNamed:@"nextBtn.png"] forState:UIControlStateNormal ];
        [_nextBtn setImage:[UIImage imageNamed:@"nextBtnSelected.png"] forState:UIControlStateHighlighted ];
        [_nextBtn setImage:[UIImage imageNamed:@"nextBtnSelected.png"] forState:UIControlStateSelected ];
        [_nextBtn setTag:2];
        [_nextBtn addTarget:self action:@selector(slideAction:) forControlEvents:UIControlEventTouchUpInside];
        _nextBtn.titleLabel.text = @"next";
        [_nextBtn setHidden:YES];
        [self addSubview:_nextBtn];
        
        _noteBtn = [UIButton new];
        [_noteBtn setTitleColor:[UIColor colorWithRed:88.0f/255.0f green:89.0f/255.0f blue:91.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [_noteBtn addTarget:self action:@selector(noteAction) forControlEvents:UIControlEventTouchUpInside];
        //[_noteBtn.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:_noteBtn];
        
        
    }
    //self.backgroundColor = [UIColor blueColor];
    return self;
}

-(void)noteAction
{
    if (self.noteDelegate != nil && [self.noteDelegate respondsToSelector:@selector(ActionNote:)])
    {
        [self.noteDelegate ActionNote:self.noteContent];
    }
}

-(void)slideAction: (id) sender
{
    if (self.slideDelegate != nil && [self.slideDelegate respondsToSelector:@selector(SlideAction:withTitle:)])
    {
        [self.slideDelegate SlideAction:((UIButton *)sender).tag withTitle:self.title.text];
    }
}

- (void)enableSlide:(BOOL)isEnalbe
{
    if (isEnalbe == YES)
    {
        //[self addSubview:self.preBtn];
        //[self addSubview:self.nextBtn];
        //_isSliding = YES;
        [self.nextBtn setHidden:NO];
        [self.preBtn setHidden:NO];
    }
    else
    {
        /*for (UIView *subView in [self subviews]) {
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *) subView;
                if (btn.titleLabel.text == self.preBtn.titleLabel.text ||
                    btn.titleLabel.text == self.nextBtn.titleLabel.text)
                {
                    [btn removeFromSuperview];
                }
            }
        }
        _isSliding = NO;*/
        [self.nextBtn setHidden:YES];
        [self.preBtn setHidden:YES];
        //[self reCaculateViewContent];
        
    }
}

- (void) closeAction:(id) sender
{
    if (self.contentDelegate != nil && [self.contentDelegate respondsToSelector:@selector(ActionContent:)] == YES)
    {
        [self.contentDelegate ActionContent:@"close"];
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGSize screenSize = [Utils screenSize];
    
    CGFloat width = screenSize.width;//self.frame.size.width - PADDING*2;
    self.bgColor.frame = CGRectMake(PADDING*2, 0.0f, width - PADDING*4, self.frame.size.height);
    
    self.closeBtn.frame = CGRectMake(width - (BUTTON_WIDTH + PADDING * 2), 0.0f, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    self.leftImage.frame = CGRectMake(PADDING*2, (self.closeBtn.frame.size.height - IMAGE_VIEW_HEIGHT) /2.0f + PADDING, IMAGE_VIEW_WIDTH, IMAGE_VIEW_HEIGHT);
    
    [self.title sizeToFit];
    self.title.frame = CGRectMake(self.leftImage.frame.size.width + PADDING*3, (self.closeBtn.frame.size.height - self.title.frame.size.height)/2.0f + PADDING, width - self.leftImage.frame.size.width + PADDING*3 - self.closeBtn.frame.size.width , self.title.frame.size.height);
    
    [self.noteBtn sizeToFit];
    
    self.noteBtn.frame = CGRectMake(self.leftImage.frame.size.width, self.frame.size.height - self.noteBtn.frame.size.height - PADDING, self.noteBtn.frame.size.width, self.noteBtn.frame.size.height);
    
    /*if (self.isSliding == NO)
    {
        
        self.contentView.frame = CGRectMake(self.title.frame.origin.x, self.title.frame.size.height + self.title.frame.origin.y + PADDING, self.bgColor.frame.size.width - self.closeBtn.frame.size.width - self.leftImage.frame.size.width, self.bgColor.frame.size.height - self.closeBtn.frame.size.height - PADDING*4- self.noteBtn.frame.size.height);
    }
    else{
        CGFloat x = self.frame.size.width - self.preBtn.frame.size.width *2 - PADDING*2;
        CGFloat y = self.noteBtn.frame.origin.y - self.preBtn.frame.size.height - PADDING;
        self.preBtn.frame = CGRectMake(x, y, self.preBtn.frame.size.width, self.preBtn.frame.size.height);
        self.nextBtn.frame = CGRectMake(self.preBtn.frame.origin.x + self.preBtn.frame.size.width + PADDING, y, self.nextBtn.frame.size.width, self.nextBtn.frame.size.height);
    }*/
    if (self.isSliding == YES)
    {
        CGFloat x =  (self.frame.size.width - BUTTON_WIDTH *2 - PADDING*2) /2.0f;
        CGFloat y = self.noteBtn.frame.origin.y - BUTTON_HEIGHT - PADDING;
        self.preBtn.frame = CGRectMake(x, y, BUTTON_WIDTH, BUTTON_HEIGHT);
        self.nextBtn.frame = CGRectMake(self.preBtn.frame.origin.x + self.preBtn.frame.size.width + PADDING, y, BUTTON_WIDTH, BUTTON_HEIGHT);
    
        self.contentView.frame = CGRectMake(self.title.frame.origin.x, self.title.frame.size.height + self.title.frame.origin.y + PADDING, self.bgColor.frame.size.width - self.closeBtn.frame.size.width - self.leftImage.frame.size.width, self.bgColor.frame.size.height - self.closeBtn.frame.size.height - PADDING*4- self.noteBtn.frame.size.height - self.preBtn.frame.size.height);
    }
    else{
        self.contentView.frame = CGRectMake(self.title.frame.origin.x, self.title.frame.size.height + self.title.frame.origin.y + PADDING, self.bgColor.frame.size.width - self.closeBtn.frame.size.width - self.leftImage.frame.size.width, self.bgColor.frame.size.height - self.closeBtn.frame.size.height - PADDING*2- self.noteBtn.frame.size.height);
        
    }
}

-(void)updateContent:(NSString *)content
{
    [self.contentView loadHTMLString:content baseURL:nil];
}

@end
