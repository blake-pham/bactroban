//
//  ViewController.m
//  Bactroban
//
//  Created by CuongPQ on 10/23/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "ViewController.h"
#import "ConstVal.h"
#import "ProductView.h"
#import "MainContentView.h"
#import "NSURLImageProtocol.h"
#import "BasePopupView.h"
#import "MainContentInformation.h"
#import "Utils.h"

@interface ViewController () <ActionDelegate, ProductActionDelegate, ContentActionDelegate, BasePopupAction, PIActionDelegate>


@property (nonatomic, strong) UIView *mainView;

#pragma define ui for home page.
@property (nonatomic, strong) UILabel *homeTitle;

@property (nonatomic, strong) UIImageView *homeImage;

@property (nonatomic, assign) BOOL isHome;
// end home page.

#pragma define ui for product view page.
@property (nonatomic, strong) UIImageView *productImage;
@property (nonatomic, strong) ProductView *productView;
@property (nonatomic, assign) BOOL isProductView;
//end product view page.

#pragma define ui for contentView.
@property (nonatomic, strong) NSString *lastFeatureAccess;
@property (nonatomic, strong) MainContentView *pageContentView;
@property (nonatomic, assign) BOOL isFeatureView ;
@property (nonatomic, strong) NSMutableArray *dataSource;


#pragma define page popupview.
@property (nonatomic, strong) BasePopupView *poupView;

#pragma define pi view
@property (nonatomic, strong) MainContentInformation *PiView;
@property (nonatomic, assign) BOOL isPi;
@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [NSURLProtocol registerClass:[NSURLImageProtocol class]];
    // Do any additional setup after loading the view, typically from a nib.
    [self resetAllFlag];
    CGSize screenSize = [Utils screenSize];
    _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height)];
    _mainView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

    _mainView.backgroundColor = [UIColor whiteColor];
    [_mainView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_land.png"]] ];
    self.view = _mainView;
    
    CGFloat statusBarHeight = 20.0f;//[UIApplication sharedApplication].statusBarFrame.size.height;

    self.headerViewLayout = [[HeaderView alloc] initWithFrame:CGRectMake(0, statusBarHeight  , screenSize.width, HEADER_HEIGHT)];
    
    [_mainView addSubview:self.headerViewLayout];
    
    
    self.footerViewLayout = [[FooterView alloc] initWithFrame:CGRectMake(0, screenSize.height - FOOTER_HEIGHT , screenSize.width, FOOTER_HEIGHT)];
    [_mainView addSubview:self.footerViewLayout];
    
    self.contentView = [[UIView alloc] initWithFrame:CGRectMake(PADDING, self.headerViewLayout.frame.size.height , screenSize.width - PADDING*2, screenSize.height - self.headerViewLayout.frame.size.height - self.footerViewLayout.frame.size.height) ];
    
    self.contentView.layer.cornerRadius = 30.0f;

    _homeTitle = [UILabel new];
    [_homeTitle setText:@"KHÁNG SINH BÔI NGOÀI DA"];
    [_homeTitle setFont: [UIFont fontWithName:@"TrebuchetMS-Bold" size:25.0f]];
    [_homeTitle sizeToFit];
    
    _homeImage = [UIImageView new];
    _homeImage.image = [UIImage imageNamed:@"product_icon.png"];
    [_homeImage setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_homeImage addGestureRecognizer:singleTap];
    [_homeImage sizeToFit];
    
    self.headerViewLayout.actionDo = self;
    //NSLog(@"go go");
    
    _productImage = [UIImageView new];
    _productImage.image = [UIImage imageNamed:@"portrai_batroban.png"];
    [_productImage sizeToFit];
    _productView = [ProductView new];
    _productView.productDelegate = self;
    
    //define datasource
    _dataSource = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"contentData" ofType:@"plist"]];
    //NSLog(@"loaded data source");
    
}

-(void)initPageFeatureView:(BOOL)enableSlide
{
    //_pageContentView = [MainContentHeaderView new];
    _pageContentView = [[MainContentView alloc] initWithSliding:enableSlide];
    _pageContentView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _pageContentView.contentDelegate = self;
    _pageContentView.noteDelegate = self;
    _pageContentView.slideDelegate = self;
    _isFeatureView = NO;
}

-(void)initPiView
{
    [self removeAllContent];
    _PiView = [MainContentInformation new];
    _PiView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _PiView.contentDelegate = self;
    
    [self.headerViewLayout resetBtnSate];
    [self.headerViewLayout.piBtn setSelected:YES];
    [self.mainView addSubview:_PiView];
    
    CGSize screenSize = [Utils screenSize];
    
     CGFloat contentHight = screenSize.height - self.headerViewLayout.frame.origin.y - self.headerViewLayout.frame.size.height - self.footerViewLayout.frame.size.height - PADDING*2;
    _PiView.frame = CGRectMake(0.0f, self.headerViewLayout.frame.origin.y + self.headerViewLayout.frame.size.height + PADDING, screenSize.width, contentHight - PADDING);
}


-(void)resetAllFlag
{
    _isHome = NO;
    _isProductView = NO;
    _isFeatureView = NO;
    _isPi = NO;
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    [self.homeTitle removeFromSuperview];
    [self.homeImage removeFromSuperview];
    
    //set home button not selected.
    [self.headerViewLayout.homeBtn setSelected:NO];
    [self initProductView];
}

#pragma init product view with feature action.
-(void) initProductView
{
    CGSize screenSize = [Utils screenSize];
    [self resetAllFlag];
    [self removeAllContent];
    [self.headerViewLayout resetBtnSate];
    
    self.isProductView = YES;
    //add product iamge view into controllerView.
    self.productImage.frame = CGRectMake((screenSize.width - self.productImage.frame.size.width)/2.0f, self.homeTitle.frame.size.height + self.homeTitle.frame.origin.y + PADDING*2, self.productImage.frame.size.width, self.productImage.frame.size.height);
    [self.mainView addSubview:self.productImage];
    
    self.productView.frame = self.productImage.frame;
    [self.mainView addSubview:self.productView];
}

-(void) viewDidAppear:(BOOL)animated
{
    CGSize screenSize = [Utils screenSize];
    //NSLog(@"did applear height: %f", self.headerViewLayout.frame.size.height);
    self.contentView.frame = CGRectMake(PADDING, self.headerViewLayout.frame.size.height + self.headerViewLayout.frame.origin.y + PADDING , screenSize.width - PADDING*2, screenSize.height - self.headerViewLayout.frame.size.height - self.headerViewLayout.frame.origin.y - self.footerViewLayout.frame.size.height - PADDING *2);
    //[self.view addSubview:self.contentView];
    [self initHomePage];
}

-(void) initHomePage
{
    CGSize screenSize = [Utils screenSize];
    [self resetAllFlag];
    self.isHome = YES;
    
    [self.mainView addSubview:self.homeTitle];
    self.homeTitle.frame = CGRectMake((screenSize.width - self.homeTitle.frame.size.width)/2.0f, self.headerViewLayout.frame.size.height + self.headerViewLayout.frame.origin.y + PADDING*2, self.homeTitle.frame.size.width, self.homeTitle.frame.size.height);
    
    [self.mainView addSubview:self.homeImage];
    
    self.homeImage.frame = CGRectMake((screenSize.width - self.homeImage.frame.size.width)/2.0f, self.homeTitle.frame.size.height + self.homeTitle.frame.origin.y + PADDING*2, self.homeImage.frame.size.width, self.homeImage.frame.size.height);
    
    // set home button selected.
    [self.headerViewLayout resetBtnSate];
    [self.headerViewLayout.homeBtn setSelected:YES];
}

-(void)changeContent:(NSString *)state
{
    if ([state isEqualToString: READING] && self.headerViewLayout.readingBtn.selected == NO)
    {
        if (  self.lastFeatureAccess != nil && [self.lastFeatureAccess isEqualToString:@""] == NO)
        {
            [self initFeatureView:self.lastFeatureAccess withContent:@"" andNote:@""];
        }
    }
    else if ([state isEqualToString: PI] && self.headerViewLayout.piBtn.selected == NO)
    {
        NSMutableArray *piDataSource = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PIData" ofType:@"plist"]];
        self.isPi = YES;
        [self initPiView];
        
        if (piDataSource != nil && [piDataSource count] > 0)
        {
            self.PiView.title.text = [piDataSource objectAtIndex:0];
            NSString *htmlData = @"";
            for (NSInteger index = 1; index < [piDataSource count]; index++) {
                htmlData = [htmlData stringByAppendingFormat:@"%@", [piDataSource objectAtIndex: index]];
            }
            [NSString stringWithFormat:@"<html><body>%@</body></html>",htmlData];
            [self.PiView updateContent:htmlData];
        }
    }
    else if ([state isEqualToString:HOME] && self.headerViewLayout.homeBtn.selected == NO){
        
        [self removeAllContent];
        [self initHomePage];
        
    }
}

-(void)removeAllContent
{
    for (UIView *item in self.mainView.subviews) {
        if ([item isKindOfClass:[HeaderView class]] || [item isKindOfClass: [FooterView class]])
        {
            continue;
        }
        else
        {
            [item removeFromSuperview];
        }
    }
    //self.lastFeatureAccess = @"";
    [self resetAllFlag];
}

-(void) viewWillAppear:(BOOL)animated
{
    //NSLog(@"hegith : %f", self.headerViewLayout.frame.size.height);
    //self.contentView.frame = CGRectMake(PADDING, self.headerViewLayout.frame.size.height , [[UIScreen mainScreen] bounds].size.width - PADDING*2, [[UIScreen mainScreen] bounds].size.height - self.headerViewLayout.frame.size.height - self.footerViewLayout.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma rotate.
-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"will rotate");
    [self beforeRotate];
}

-(void)beforeRotate
{
    
    for (UIView *viewItem in self.mainView.subviews) {
        if ([viewItem isKindOfClass:[HeaderView class]] || [viewItem isKindOfClass: [FooterView class]])
        {
            continue;
        }
        
        else{
            if (self.isHome == YES || self.isProductView == YES)
            {
                [viewItem removeFromSuperview];
            }
        }
    }
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    NSLog(@"rotated");
    [self afterRotate];
}

-(void) afterRotate
{
    CGSize screenSize = [Utils screenSize];
    if (self.isHome == YES)
    {
        self.isHome = NO;
        [self initHomePage];
    }
    else if (self.isProductView == YES)
    {
        self.isProductView = NO;
        [self initProductView];
    }
    else if (self.isFeatureView == YES)
    {
        CGFloat contentHight = screenSize.height - self.headerViewLayout.frame.origin.y - self.headerViewLayout.frame.size.height - self.footerViewLayout.frame.size.height - PADDING*2;
        self.pageContentView.frame = CGRectMake(0.0f, self.headerViewLayout.frame.origin.y + self.headerViewLayout.frame.size.height + PADDING, screenSize.width, contentHight - PADDING);
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // Will rotate
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        // Will animate rotation
        [self beforeRotate];
        NSLog(@"will be rotate");
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        // Did rotate
        NSLog(@"did rotate");
        [self afterRotate];
    }];
}

//
-(void) initFeatureView:(NSString *)title withContent:(NSString *)content andNote:(NSString *)note
{
    
    
    [self removeAllContent];
    [self.headerViewLayout resetBtnSate];
    [self.headerViewLayout.readingBtn setSelected:YES];
    self.isFeatureView = YES;
    [self initPageFeatureView:YES];
    if ([title isEqualToString:TAC_DUNG_KHONG_MONG_MUON])
    {
        
        [self.pageContentView enableSlide:YES];
        [self.pageContentView.nextBtn setSelected:YES];
    }
    else if ([title isEqualToString:THAN_TRONG])
    {
        [self.pageContentView enableSlide:YES];
        [self.pageContentView.nextBtn setSelected:YES];
        [self.pageContentView.preBtn setSelected:YES];
    }
    else if ([title isEqualToString:THAI_KI_VA_CHO_CON_BU])
    {
        [self.pageContentView enableSlide:YES];
        [self.pageContentView.preBtn setSelected:YES];
    }
    else{
        [self initPageFeatureView:NO];
        [self.pageContentView enableSlide:NO];
    }
    
    CGSize screenSize = [Utils screenSize];
    
    CGFloat contentHight = screenSize.height - self.headerViewLayout.frame.origin.y - self.headerViewLayout.frame.size.height - self.footerViewLayout.frame.size.height - PADDING*2;
    self.pageContentView.title.text = title;
    self.pageContentView.frame = CGRectMake(0.0f, self.headerViewLayout.frame.origin.y + self.headerViewLayout.frame.size.height + PADDING, screenSize.width, contentHight - PADDING);
    
    
    //NSString *imageTag = [NSString stringWithFormat:@"<img src=\"%@://co_che_tac_dung_phu.png\" />",kProtocolImageUrl];
    //NSString *testHtml = [NSString stringWithFormat:@"<html><body><font size='5' color='blue'><b>&FilledSmallSquare; custom protocol</b><br/><b>&ShortRightArrow; hello <font color='red'> goodbye</font></b></font><br/>%@<body></html>",imageTag];
    
    NSString *val =[self getContentDataFromTitle:title];
    [self.pageContentView updateContent: val];
    NSMutableArray *noteData = [self getNoteFromTitle:title];
    [self.pageContentView.noteBtn setTitle:[noteData objectAtIndex:0] forState:UIControlStateNormal];
    self.pageContentView.noteContent = ([noteData count] > 1) ? [noteData objectAtIndex:1]: @"";
    
    [self.mainView addSubview:self.pageContentView];
    self.lastFeatureAccess = title;
    
}

- (NSString *)getContentDataFromTitle: (NSString *) title
{
    NSString *data =@"";
    if (self.dataSource != nil && title != nil && [title isEqualToString:@""] == NO)
    {
        for (NSMutableArray *arr in self.dataSource) {
            
            if ( arr != nil && [arr count] > 0 && [[arr objectAtIndex:0] isEqualToString:title])
            {
                for (NSInteger index =1 ; index < [arr count] -1; index ++) {
                    data = [data stringByAppendingFormat:@"%@", [arr objectAtIndex:index]];
                }
                break;
            }
        }
    }
    data = [NSString stringWithFormat:@"<html><body>%@</body></html>", data];
    return data;
}

- (NSMutableArray *)getNoteFromTitle: (NSString *) title
{
    //NSString *data =@"";
    if (self.dataSource != nil && title != nil && [title isEqualToString:@""] == NO)
    {
        for (NSMutableArray *arr in self.dataSource) {
            
            if ( arr != nil && [arr count] > 0 && [[arr objectAtIndex:0] isEqualToString:title])
            {
                if ([[arr objectAtIndex:[arr count] -1] isKindOfClass:[NSMutableArray class]])
                {
                    return [arr objectAtIndex:[arr count] -1];
                }
                break;
            }
        }
    }

    return nil;
}

#pragma handle action click in header view.
-(void)ActionDelegate:(NSString *)state
{
    if ([state isEqualToString:@""] == false)
    {
        //NSLog(@"change page with action %@",  state);
        [self changeContent:state];
    }
}

#pragma handle action product
-(void)FeatureAction:(NSInteger)index
{
    NSString *featureName = @"";
    switch (index) {
        case 1:
            featureName = CHI_DINH;
            break;
        case 2:
            featureName = CO_CHE_TAC_DUNG;
            break;
        case 3:
            featureName = PHO_KHANG_KHUAN;
            break;
        case 4:
            featureName = TINH_NHAY_CAM_VI_KHUAN;
            break;
        case 5:
            featureName = HIEU_QUA_SO_VOI_ERYTHROMYCIN;
            break;
        case 6:
            featureName = HIEU_QUA_SO_VOI_GENTAMICIN;
            break;
        case 7:
            featureName = TAC_DUNG_KHONG_MONG_MUON;
            break;
            
        default:
            featureName = @"";
            break;
    }
    if ([featureName isEqualToString:@""] == NO)
    {
        [self initFeatureView:featureName withContent:@"" andNote:@""];
    }
    
}

#pragma content action delegate.
-(void) ActionContent:(NSString *)actionName
{
    if ([actionName isEqualToString:@""] == NO)
    {
        [self initProductView];
    }
}

#pragma action note delegate.
-(void) ActionNote:(NSString *)noteContent
{
    self.poupView = [[BasePopupView alloc] initWithFrame:self.mainView.frame];
    self.poupView.popupDelegate = self;
    self.poupView.contentNote.text = noteContent;
    
    [self.view addSubview:self.poupView];
    
}

#pragma action touch up out side of popup.
-(void) TouchOutSidePopUpView
{
    //NSLog(@"here");
    [self.poupView removeFromSuperview];
}

#pragma action slide.
-(void) SlideAction:(NSInteger)index withTitle:(NSString *)title
{
    NSString *newTitle=@"";
    if (index == 1)
    {
        if ([title isEqualToString: THAI_KI_VA_CHO_CON_BU])
        {
            newTitle = THAN_TRONG;
        }
        else if ([title isEqualToString: THAN_TRONG])
        {
            newTitle = TAC_DUNG_KHONG_MONG_MUON;
        }
    }
    else if (index ==2)
    {
        if ([title isEqualToString:TAC_DUNG_KHONG_MONG_MUON])
        {
            newTitle = THAN_TRONG;
        }
        else if ([title isEqualToString:THAN_TRONG])
        {
            newTitle = THAI_KI_VA_CHO_CON_BU;
        }
    }
    
    if ([newTitle isEqualToString:@""] == NO)
    {
        [self initFeatureView:newTitle withContent:@"" andNote:@""];
    }
}

#pragma handle pi action delegate
-(void)ActionPIContent:(NSString *)actionName
{
    if ([actionName isEqualToString:@""] == NO)
    {
        [self initProductView];
    }
}

@end
