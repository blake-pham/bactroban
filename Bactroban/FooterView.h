//
//  FooterView.h
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterView : UIView

@property (nonatomic, strong) UILabel *leftTitle;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *rightLabel;

-(void) updateContent:(NSString *) leftContent contentData:(NSString*) content right:(NSString *)rightContnet;

@end
