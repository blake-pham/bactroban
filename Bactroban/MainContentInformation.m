//
//  MainContent.m
//  Bactroban
//
//  Created by CuongPQ on 10/24/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "MainContentInformation.h"
#import "ConstVal.h"
#import "Utils.h"

@interface MainContentInformation()
@property (nonatomic, strong) UIView *bgColor;

@end
@implementation MainContentInformation

@synthesize leftImage = _leftImage;
@synthesize title = _title;
@synthesize closeBtn = _closeBtn;
@synthesize contentView = _contentView;


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _bgColor = [[UIView alloc] initWithFrame:CGRectMake(PADDING*2, 0.0f, self.frame.size.width - PADDING*2, self.frame.size.height)];
        _bgColor.backgroundColor = [UIColor whiteColor];
        _bgColor.layer.cornerRadius = 30.0f;
        
        [self addSubview:_bgColor];
        
        _leftImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"content_sign.png"]];
        [self addSubview:_leftImage];
        
        _title =[UILabel new];
        [_title setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:25.0f]];
        [_title setTextColor:[UIColor redColor]];
        [self addSubview:_title];
        
        _closeBtn = [UIButton new];
        //width and height should be same value
        //_closeBtn.frame = CGRectMake(0, self.frame.origin.y, BUTTON_WIDTH, BUTTON_HEIGHT);
        //Clip/Clear the other pieces whichever outside the rounded corner
        _closeBtn.clipsToBounds = YES;
        [_closeBtn setImage:[UIImage imageNamed:@"closeBtn.png"] forState:UIControlStateNormal ];
        [_closeBtn setTag:1];
        [_closeBtn addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
        //half of the width
        _closeBtn.layer.cornerRadius = BUTTON_WIDTH/2.0f;
        // _closeBtn.layer.borderColor = (__bridge CGColorRef)([UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:161.0f/255.0f alpha:0.7f]);
        //_closeBtn.layer.borderWidth=2.0f;
        [self addSubview:_closeBtn];
        
        _contentView = [UIWebView new];
        _contentView.contentMode = UIViewContentModeScaleAspectFit;
        [_contentView setBackgroundColor:[UIColor clearColor]];
        _contentView.scrollView.scrollEnabled = YES;
        [_contentView setClipsToBounds:YES];
        _contentView.layer.cornerRadius = 30.0f;
        UIScrollView *scrollView = [_contentView.subviews objectAtIndex:0];
        [scrollView flashScrollIndicators];
        

        [self addSubview:_contentView];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGSize screenSize = [Utils screenSize];
    CGFloat width = screenSize.width;//self.frame.size.width - PADDING*2;
    self.bgColor.frame = CGRectMake(PADDING*2, 0.0f, width - PADDING*4, self.frame.size.height);
    
    self.closeBtn.frame = CGRectMake(width - (BUTTON_WIDTH + PADDING * 2), 0.0f, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    self.leftImage.frame = CGRectMake(PADDING*2, (self.closeBtn.frame.size.height - IMAGE_VIEW_HEIGHT) /2.0f + PADDING, IMAGE_VIEW_WIDTH, IMAGE_VIEW_HEIGHT);
    
    [self.title sizeToFit];
    self.title.frame = CGRectMake(self.leftImage.frame.size.width + PADDING*3, (self.closeBtn.frame.size.height - self.title.frame.size.height)/2.0f + PADDING, width - self.leftImage.frame.size.width + PADDING*3 - self.closeBtn.frame.size.width , self.title.frame.size.height);

     self.contentView.frame = CGRectMake(PADDING*2, self.title.frame.size.height + self.title.frame.origin.y + PADDING, self.bgColor.frame.size.width , self.frame.size.height - self.closeBtn.frame.size.height -PADDING);
 
}

- (void) closeAction:(id) sender
{
    if (self.contentDelegate != nil && [self.contentDelegate respondsToSelector:@selector(ActionPIContent:)] == YES)
    {
        [self.contentDelegate ActionPIContent:@"close"];
    }
}

-(void)updateContent:(NSString *)content
{
    [self.contentView loadHTMLString:content baseURL:nil];
}
@end
