//
//  HeaderView.m
//  Bactroban
//
//  Created by CuongPQ on 10/23/14.
//  Copyright (c) 2014 com.cpq. All rights reserved.
//

#import "HeaderView.h"
#import "ConstVal.h"
#import "Utils.h"

@interface HeaderView() <ActionDelegate>

@property (nonatomic,assign) CGFloat currentHeight;

@end

@implementation HeaderView

@synthesize leftLabel = _leftLabel;
@synthesize header = _header;
@synthesize subHeader = _subHeader;
@synthesize subHeaderNote = _subHeaderNote;
@synthesize tradeMark = _tradeMark;
@synthesize readingBtn = _readingBtn;
@synthesize piBtn = _piBtn;
@synthesize homeBtn = _homeBtn;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _leftLabel = [UILabel new];
        [_leftLabel setText:@"Tài liệu thông tin cho cán bộ y tế"];
        _leftLabel.textColor = [UIColor blackColor];
        [_leftLabel setFont:[UIFont systemFontOfSize:12.0f]];
        [self addSubview:_leftLabel];
        
        _header = [UILabel new];
        [_header setText:@"Bactroban"];
        [_header setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:40.0f]];
        _header.textColor = [UIColor blackColor];
        [self addSubview:_header];
        
        _subHeader = [UILabel new];
        [_subHeader setText:@"Ointment"];
        [_subHeader setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:14.0f]];
        _subHeader.textColor = [UIColor blackColor];
        [self addSubview:_subHeader];
        
        _subHeaderNote = [UILabel new];
        [_subHeaderNote setText:@"(mupirocin 2%)"];
        [_subHeaderNote setFont:[UIFont systemFontOfSize:14.0f]];
        _subHeaderNote.textColor = [UIColor blackColor];
        [self addSubview:_subHeaderNote];
        
        _tradeMark = [UILabel new];
        [_tradeMark setText:@"®"];
        [_tradeMark setFont:[UIFont systemFontOfSize:9.0f]];
         _tradeMark.textColor = [UIColor blackColor];
        [self addSubview:_tradeMark];
        
        _readingBtn = [UIButton new];
        //width and height should be same value
        //_readingBtn.frame = CGRectMake(0, self.frame.origin.y, BUTTON_WIDTH, BUTTON_HEIGHT);
        //Clip/Clear the other pieces whichever outside the rounded corner
        _readingBtn.clipsToBounds = YES;
        [_readingBtn setImage:[UIImage imageNamed:@"readingBtn.png"] forState:UIControlStateNormal];
        [_readingBtn setImage:[UIImage imageNamed:@"readingBtnSelected.png"] forState:UIControlStateHighlighted];
        [_readingBtn setImage:[UIImage imageNamed:@"readingBtnSelected.png"] forState:UIControlStateSelected];
        [_readingBtn setTag:1];
        [_readingBtn addTarget:self action:@selector(hearButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        //half of the width
        _readingBtn.layer.cornerRadius = BUTTON_WIDTH/2.0f;
        //_readingBtn.layer.borderColor = [[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:161.0f/255.0f alpha:0.7f] CGColor];
        //_readingBtn.layer.borderWidth=2.0f;
        //_readingBtn.backgroundColor = [UIColor redColor];
        [self addSubview:_readingBtn];
        
        _piBtn = [UIButton new];
        //width and height should be same value
        //_piBtn.frame = CGRectMake(0, self.frame.origin.y, BUTTON_WIDTH, BUTTON_HEIGHT);
        [_piBtn setImage:[UIImage imageNamed:@"piBtn.png"] forState:UIControlStateNormal];
        [_piBtn setImage:[UIImage imageNamed:@"piBtnSelected.png"] forState:UIControlStateHighlighted];
        [_piBtn setImage:[UIImage imageNamed:@"piBtnSelected.png"] forState:UIControlStateSelected];
        
        [_piBtn setTag:2];
        [_piBtn addTarget:self action:@selector(hearButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        //Clip/Clear the other pieces whichever outside the rounded corner
        _piBtn.clipsToBounds = YES;
        //half of the width
        _piBtn.layer.cornerRadius = BUTTON_WIDTH/2.0f;
        //_piBtn.layer.borderColor = [[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:161.0f/255.0f alpha:0.7f] CGColor];
        //_piBtn.layer.borderWidth=3.0f;
        //_piBtn.backgroundColor = [UIColor redColor];
        [self addSubview:_piBtn];
        
        _homeBtn = [UIButton new];
        //width and height should be same value
        //_homeBtn.frame = CGRectMake(0, self.frame.origin.y, BUTTON_WIDTH, BUTTON_HEIGHT);
        [_homeBtn setImage:[UIImage imageNamed:@"homeBtn.png"] forState:UIControlStateNormal];
        [_homeBtn setImage:[UIImage imageNamed:@"homeBtnSelected.png"] forState:UIControlStateHighlighted];
        
        [_homeBtn setImage:[UIImage imageNamed:@"homeBtnSelected.png"] forState:UIControlStateSelected];
        
        [_homeBtn setTag:3];
        [_homeBtn addTarget:self action:@selector(hearButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        //Clip/Clear the other pieces whichever outside the rounded corner
        _homeBtn.clipsToBounds = YES;
        //half of the width
        _homeBtn.layer.cornerRadius = BUTTON_WIDTH/2.0f;
        _homeBtn.backgroundColor = [UIColor clearColor];
        //_homeBtn.layer.borderColor = [[UIColor colorWithRed:131.0f/255.0f green:150.0f/255.0f blue:161.0f/255.0f alpha:0.7f] CGColor];
        //_homeBtn.layer.borderWidth=3.0f;
        //_homeBtn.backgroundColor = [UIColor redColor];
        [self addSubview:_homeBtn];
        
        //self.backgroundColor = [UIColor yellowColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void) hearButtonAction: (id) sender
{
    UIButton *btn = (UIButton*) sender;
    NSString *state = @"";
    if (btn.tag == 1)
    {
        NSLog(@"press into button reading");
        state = READING;
    }
    else if (btn.tag == 2)
    {
        NSLog(@"press into button pi");
        state = PI;
    }
    else{
        NSLog(@"press into button home");
        state = HOME;
    }
    
    if (self.actionDo != nil && [self.actionDo respondsToSelector:@selector(ActionDelegate:)])
    {
        [self.actionDo ActionDelegate:state];
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews ];
    CGSize screenSize = [Utils screenSize];
    CGFloat width = screenSize.width;//self.frame.size.width;
    //CGFloat height = self.frame.size.height;
    
    
    self.homeBtn.frame = CGRectMake(width - BUTTON_WIDTH - PADDING, PADDING, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    //self.currentHeight = self.homeBtn.frame.origin.y + self.homeBtn.frame.size.height;
    
    self.piBtn.frame = CGRectMake(self.homeBtn.frame.origin.x - PADDING - BUTTON_WIDTH, PADDING, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    self.readingBtn.frame = CGRectMake(self.piBtn.frame.origin.x - PADDING - BUTTON_WIDTH,  PADDING , BUTTON_WIDTH, BUTTON_HEIGHT);
    
    [self.leftLabel sizeToFit];
    
    self.leftLabel.frame = CGRectMake(PADDING, (self.homeBtn.bounds.size.height + self.homeBtn.frame.origin.y - self.leftLabel.frame.size.height)/2.0f + PADDING, self.homeBtn.frame.origin.x - PADDING*2, self.leftLabel.frame.size.height);
    
    [self.header sizeToFit];
    self.header.frame = CGRectMake((width - self.header.frame.size.width - PADDING) /2, PADDING + self.homeBtn.frame.size.height, self.header.frame.size.width, self.header.frame.size.height);
    
    [self.tradeMark sizeToFit];
    self.tradeMark.frame = CGRectMake(self.header.frame.origin.x + self.header.frame.size.width, self.header.frame.origin.y, self.tradeMark.frame.size.width, self.tradeMark.frame.size.height);
    
    [self.subHeader sizeToFit];
    self.subHeader.frame = CGRectMake(self.header.frame.origin.x, self.header.frame.origin.y + self.header.frame.size.height , self.subHeader.frame.size.width, self.subHeader.frame.size.height);
    
    [self.subHeaderNote sizeToFit];
    self.subHeaderNote.frame = CGRectMake(self.subHeader.frame.origin.x + self.subHeader.frame.size.width + PADDING, self.subHeader.frame.origin.y, self.subHeaderNote.frame.size.width, self.subHeaderNote.frame.size.height);
    self.heightVal = self.subHeader.frame.origin.y + self.subHeader.frame.size.height + PADDING;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.subHeader.frame.origin.y + self.subHeader.frame.size.height + PADDING);
    //NSLog(@"new height: %f", self.frame.size.height);
}

-(CGFloat) updateCurrentHeightLayout
{
    return self.currentHeight;
    //NSLog(@"h=%f", self.currentHeight);
}

-(void) resetBtnSate
{
    [self.readingBtn setSelected:NO];
    [self.piBtn setSelected:NO];
    [self.homeBtn setSelected:NO];
}

@end
